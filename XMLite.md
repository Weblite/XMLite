# Extensible Markup Language, Lightweight Specification, Draft 1

The Extensible Markup Language (XML) is a meta-language and data model for *markup languages*, information formats for rich documents with embedded metadata. This specification, also known as "XMLite," describes a subset of XML intended to cover most common use cases while avoiding the parts of the full XML specification that are especially difficult or complicated for parsers to implement. It is divided into two parts: The Extensible Markup Language Lightweight Data Model (XMLite-Model) describes the abstract model of information XMLite represents, while the Extensible Markup Language Lightweight Syntax (XMLite-Syntax) describes the syntax of XML to enable it to be parsed into XMLite-Model data.

Please note that while XMLite describes a large subset of XML, it does not include all possible XML. While documents compliant with XMLite should be correct XML documents, not all XML documents can be understood by XMLite parsers. The full XML standard is much more extensive and implementing XMLite does not make a parser fully XML-capable.

## Conventions

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL" in this document are to be interpreted as described in [[BCP 14](https://www.rfc-editor.org/info/bcp14)] [[RFC2119](https://www.rfc-editor.org/rfc/rfc2119)] [[RFC8174](https://www.rfc-editor.org/rfc/rfc8174)] when, and only when, they appear in all capitals, as shown here.

## Definitions

<dl>
  <dt>Character</dt>
  <dd>A Unicode code point.</dd>
  <dt>String</dt>
  <dd>A sequence of zero or more characters.</dd>
  <dt>Whitespace</dt>
  <dd>One or more of the characters U+0020 (space), U+000A (newline), and U+0009 (tab).</dd>
  <dt>Unqualified name</dt>
  <dd>A string of one or more characters that SHALL NOT include characters with major class C (control characters), whitespace, or characters that are below U+0080 (part of Basic Latin AKA ASCII) and are other than lower- and upper-case Latin letters, Arabic numerals, `_`, `.`, and `-`. The first character SHALL NOT be `.` or `-`. Unqualified names SHOULD be treated as case insensitive.</dd>
  <dt>Qualified name</dt>
  <dd>A pair of strings: The namespace, which SHALL NOT include characters with major class C or whitespace, and an unqualified name, called the local name. The namespace SHOULD be treated as case insensitive.</dd>
</dl>

## XMLite-Model

### Elements

The most important part of XML documents is *elements*. Each element SHALL have the following parts:

* A qualified name.
* Zero or more unordered attributes. Each attribute consists of a qualified name and a value, which is a string. The absence of an attribute is distinct from a value containing zero characters. An element SHALL NOT have more than one attribute with any given name.
* Zero or more ordered content parts. Having zero content parts is not distinct from having only a zero-character string as contents.

### Content parts

Content parts are either elements, text, processing instructions, or comments. The parser MUST produce elements, text parts, and processing instruction parts, and MAY produce comment parts.

### Text

*Text parts* are strings. Two text parts MUST NOT be adjacent; the parser MUST concatenate them into a single text part.

### Processing instructions

A *processing instruction part* provides application-specific information on how to handle a document or part of one. A processing instruction consists of an unqualified name and a string, its content. Processing instruction names beginning with `xml`, case-insensitively, are reserved. No further structure is defined.

### Comments

*Comment parts* consist of a string, which has no meaning and SHOULD NOT be examined by the application. If a parser includes them, the application SHOULD use them only to pass them through to output documents.

### Documents

A document is the primary independent unit of XML. A document contains one or more content parts. It MUST contain exactly one element. It MUST NOT contain any text parts.

### The XML declaration

The *XML declaration* (unlike in full XML) is a processing instruction with the name `xml`. If present, it MUST be at the beginning of the document. Its content consists of one or more pseudo-attributes, which are separated by whitespace. There MAY be whitespace at the beginning or end of the content, which SHALL be ignored. Each pseudo-attribute is as follows:

* The pseudo-attribute's name, exactly.
* Optional whitespace.
* An `=` character.
* Optional whitespace.
* Two `"` characters, one on each side of a string of non-`"` characters, or two `'` characters, one on each side of a string of non-`'` characters. The string is the value.

If it is present, the declaration SHALL have a pseudo-attribute with the name `version`. It SHOULD have a pseudo-attribute with the name `encoding`. It MAY have a pseudo-attribute with the name `standalone`. These pseudo-attributes MUST be in the specified order, `version`, then optionally `encoding`, then optionally `standalone`.

The value of `version` indicates the version of XML. It is normally `1.0` or `1.1`; there is no difference between the two. A parser MAY choose to signal an error on encountering another `version` value.

The value of `encoding` names, case-insensitively and entirely in ASCII characters, a registered text encoding that would be a valid value for `charset` in a media type, indicating the encoding used for the document. This value MUST be either removed or changed if the document is serialized in a different encoding.

The value of `standalone` has no meaning.

## XMLite-Syntax

### Encodings

Parsers MUST support UTF-8, and MAY support as many other encodings as they wish.

The following rules SHOULD be used to determine the text encoding of an XML document, unless only UTF-8 is supported:

1. If the parser supports UTF-16 or UTF-32, and a byte order mark for one of those encodings is found at the beginning of the document, that encoding is used.
2. If an external encoding declaration specifies an encoding, that encoding is used.
3. The parser SHOULD try to speculatively decode a small part of the beginning of the document with each supported encoding, starting with UTF-8, until it successfully finds an XML declaration; the encoding specified by the `encoding` pseudo-attribute of that declaration is used.
4. The default encoding, UTF-8, is used.

The XML declaration MUST NOT indicate a different encoding than a byte order mark or external encoding declaration does.

The parser MUST discard any byte order mark at the beginning of the document before continuing parsing.

### Basic structure

An XML document is a series of characters, and is parsed as a series of *tags* mixed with *text*. A tag is a sequence beginning with `<`. There are several types of tag; each defines its own rules for how to parse it, including when the tag ends. Different types of tags are distinguished by the characters immediately after the `<`. In all cases, when a tag ends, a `>` will be the last character, but there are circumstances where `>` can occur before the end of a tag.

### Parsing model

This specification describes the parsing process in terms of "adding" *parts*, appending them to the list of content parts of the element on top of a stack, or if the stack is empty to the document; the parser need not be implemented this way, but it MUST be implemented so as to produce equivalent results.

### Elements

An *element tag* begins with a `<` followed by a character other than `/`, `!`, or `?` and continues until either `/>` or `>`.

The string from immediately after the `<` until immediately before the first whitespace, or the ending `/>` or `>`, indicate the name of the element, which is split at the first `:` within it, if any. The first part is the *namespace prefix*, and the second the local name. If there is no `:`, the namespace prefix is the empty string and the entire name is the local name. The element's name is the namespace to which the prefix maps (see below) and the local name.

An element tag MAY contain, after whitespace following the name and before the `>`, whitespace-separated attributes. An attribute is as follows:

* A name, parsed the same as the element name. However, if the namespace prefix is the empty string, the attribute's name has the namespace of the element.
* Optional whitespace.
* An `=` character.
* Optional whitespace
* Two `"` characters, one on each side of a string of non-`"` characters, or two `'` characters, one on each side of a string of non-`'` characters. The string, after escape replacements, is the value.

There may be whitespace between the list of attributes and the end of the element tag.

If an element tag ends with `/>`, the element has no contents and is added immediately. Otherwise, the element is pushed onto the top of the stack, and all parts are added to its content until it ends.

### Namespace prefixes and mapping

Namespaces are associated with elements and attributes by namespace prefixes. Each element has a mapping from prefixes to full namespace names, which applies to itself, its attributes, and its content.

When parsing an element, the parser SHALL begin with a namespace mapping identical to that of the containing element, or a mapping only from the empty-string prefix to the empty-string namespace for the root element. If the element has the attribute `xmlns` with no namespace prefix, its value SHALL replace the current mapping for the empty namespace prefix. If the element has any attributes with the special namespace prefix `xmlns`, their values SHALL replace the current mappings, or create new mappings, for the namespace prefixes given by their local names.

The parser MUST NOT discard any of these attributes unless so directed by the application.

The namespace prefix `xml` is predefined as referring to `http://www.w3.org/XML/1998/namespace` and MUST NOT be redefined.

### End tags

An end tag begins with `</`. It is followed by a string of non-whitespace characters, optional whitespace, and ends at `>`. The string is the name of the tag, and MUST match the name of the previous element tag. The end tag causes the element to end, at which point it is removed from the stack and added as a part to the content of the previous element on the stack, or the document if none. Following the end tag, parts are added to the previous, now top, element on the stack, or to the document if none.

### Text

All characters that are not part of a tag make up text parts. Text parts SHALL be added after performing escape replacements. Text parts MUST NOT be added to the document, only to an element.

### Processing instructions

A processing instruction tag begins with `<?` and continues until `?>`. The string between the start and end characters consists of a sequence of non-whitespace characters followed optionally by a whitespace and further characters. The first sequence is the unqualified name of a processing instruction whose content is the second sequence, and these SHALL be added as a processing instruction part.

### Comments

A comment tag begins with `<!--` and continues until `-->`. They MUST NOT otherwise include `--`. It has no meaning.

The parser MAY store the string between the start and end of the comment and add it as a comment part.

### CDATA

A CDATA tag begins with `<![CDATA[` and continues until `]]>`. The characters between the start and end of the CDATA tag MUST be added as a text part, and MUST NOT be parsed in any further way, including escape replacements. The CDATA text MUST NOT be concatenated with adjacent text parts until after those parts have had their escape replacements performed.

### Vestigial SGML

A vestigial SGML tag begins with `<!` but none of the other characters defined to follow `<!` by other tag types. Vestigial SGML has no meaning and SHOULD be entirely ignored. The parser SHOULD follow this algorithm, or an equivalent one, whenever encountering a vestigial SGML tag:

1. Begin with the character after `<!`.
2. Initialize a *nesting counter* to 1.
3. Advance to the next occurrence of `<`, `>`, `"`, or `'`.
4. If an occurrence of `<` is followed immediately by `!--`, advance to immediately after the next occurrence of `-->` and skip back to step 3.
5. If the occurrence is `"`, advance to immediately after the next occurrence of `"` and skip back to step 3.
6. If the occurrence is `'`, advance to immediately after the next occurrence of `'` and skip back to step 3.
7. If the occurrence is `<`, increment the nesting counter.
8. If the occurrence is `>`, decrement the nesting counter.
9. If the nesting counter is 0, the vestigial SGML tag has ended.
10. Otherwise, return to step 3.

These tags were historically used to declare information about the structure of SGML and later XML documents, and to declare additional text replacement entities. Neither of these are allowed by XMLite.

### Escape replacement

Text and attribute values can contain escapes, historically called "entities" or "entity references." The parser SHALL make the following replacements in strings requiring escape replacement:

* `&amp;` becomes `&`.
* `&quot;` becomes `"`.
* `&apos;` becomes `'`.
* `&lt;` becomes `<`.
* `&gt;` becomes `>`.
* A substring beginning `&#x` and continuing until `;` is interpreted as a hexadecimal character reference: The characters between the start and end markers are interpreted as a hexadecimal number, and the entire substring is replaced by that code point.
* A substring beginning `&#` and continuing until `;` is interpreted as a decimal character reference: The characters between the start and end markers are interpreted as a decimal number, and the entire substring is replaced by that code point.
* All other substrings beginning `&`, continuing until `;`, and containing no whitespace are deleted.

The parser MUST NOT interpret any `&` resulting from an escape replacement as indicating another escape replacement. This includes `&`s resulting from `&amp` as well as decimal or hexadecimal character references.

## Appendix A: EBNF

The following Extended Backus-Naur Form rules largely define the syntax of XML as described by XMLite:

```ebnf
Document = { DocPart }, Element, { DocPart } ;
DocPart = ProcessingInstruction | Comment | SGML ;
Element = '<', Name, Whitespace, [ AttrList, [ Whitespace ] ], ( '>', ElemContent, EndTag ) | '/>' ;
EndTag = '</', Name, [ Whitespace ], '>' ;
ElemContent = { DocPart | Element | Text } ;
AttrList = Attribute, [ Whitespace, AttrList ] ;
Attribute = Name, [ Whitespace ], '=', [ Whitespace ], ( '"', Text, '"' ) | ( "'", Text, "'" ) ;
ProcessingInstruction = '<?', Token, Whitespace, String, '?>' ;
Comment = '<!--', ( String - '--' ), '-->' ;
SGML = '<!', { Comment | SGMLNest | SGMLStr | String }, '>' ;
SGMLNest = '<', { Comment | SGMLNest | SGMLStr | String }, '>' ;
SGMLStr = '"', String, '"' | "'", String, "'" ;
Name = QualName | UnqualName ;
QualName = Prefix, ':', UnqualName ;
Prefix = Token ;
UnqualName = Token - ':' ;
Text = { String | Escape } ;
Escape = '&amp;' | '&quot;' | '&apos;' | '&lt;' | '&gt;' | HexEscape | DecEscape | ( '&', Token, ';' ) ;
HexEscape = '&#x', { HexDigit }, ';' ;
DecEscape = '&#', { Digit }, ';' ;
HexDigit = Digit | 'a' | 'A' | 'b' | 'B' | 'c' | 'C' | 'd' | 'D' | 'e' | 'f' | 'F' ;
String = { Char } ;
Token = { TokenChar } ;
TokenChar = (Char - (NonprintableChar | ASCIIChar)) | TokenASCIIChar ; (* Note that whitespace is excluded from TokenASCIIChar. *)
TokenASCIIChar = Letter | Digit | '_' | '.' | '-' ;
Whitespace = { ' ' | Tab | LineFeed } ;
Letter = 'a' | 'A' | 'b' | 'B' | 'c' | 'C' | 'd' | 'D' | 'e' | 'f' | 'F' | 'g' | 'G' | 'h' | 'H' | 'i' | 'I' | 'j' | 'J' | 'k' | 'K' | 'l' | 'L' | 'm' | 'M' | 'n' | 'N' | 'o' | 'O' | 'p' | 'P' | 'q' | 'Q' | 'r' | 'R' | 's' | 'S' | 't' | 'T' | 'u' | 'U' | 'v' | 'V' | 'w' | 'W' | 'x' | 'X' | 'y' | 'Y' | 'z' | 'Z' ;
Digit = '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' ;
Tab = ? U+0009 ? ;
LineFeed = ? U+000A ? ;
ASCIIChar = ? any Unicode code point below U+0080 ? ;
NonprintableChar = ? any Unicode code point with major category C ? - ( Tab | LineFeed ) ;
Char = ? any Unicode code point ? ;
```
